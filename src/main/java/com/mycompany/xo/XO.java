/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.xo;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class XO {
    private static String[][] table ={{"-","-","-"},{"-","-","-"},{"-","-","-"}};
    private static int r;
    private static int c;
    private static Scanner input = new Scanner(System.in);
    private static String turn = "X";
    private static int count = 0;
   
    
    public static void main(String[] args) {
        printWelcome();
        while(true) {
            printTable();
            printTurn();
            inputRowCol();
            if(checkFinish()) {
                printTable();
                printResult();
                break;
            }
            switchTurn();
        } 
    }

    private static void printWelcome() {
        System.out.println("Welcome to XO game!!!");
    }

    private static void printTable() {
        for (int row=0; row<3;row++) {
            for (int col=0;col<3;col++) {
                System.out.print(table[row][col]+" ");
            }
            System.out.println();
        }       
    }

    private static void inputRowCol() {
        System.out.print("Input(row,columm):");
        r=input.nextInt()-1;
        c=input.nextInt()-1;
        //System.out.println(r+" "+c);
        table[r][c]= turn;
        count++;
    }

    private static void printTurn() {
        System.out.println("Turn "+turn);
    }

    private static void switchTurn() {
        if(turn == "X") {
            turn = "O";
        } else {
            turn = "X";
        }        
    }

    private static boolean checkFinish() {
        if(checkWin()) {
            return true;
        }
        if(checkDraw()) {
            return true;
        }
        return false;
    }

    private static boolean checkWin() {
        if(checkHor()) {
            return true;
        }
        if(checkVer()) {
            return true;
        }
        if(checkDia()) {
            return true;
        }
        return false;
    }

    private static boolean checkDraw() {
        if(count == 9) {
            return true;
        }
        return false;
    }

    private static boolean checkHor() {
        if(table[r][0] == turn && table[r][1] == turn && table[r][2] == turn) {
            return true;
        }
        return false;
    }

    private static boolean checkVer() {
        if(table[0][c] == turn && table[1][c] == turn && table[2][c] == turn) {
            return true;
        }    
        return false;
    }

    private static boolean checkDia() {
       if(table[0][0] == turn && table[1][1] == turn && table[2][2] == turn) {
            return true;
        }
       if(table[0][2] == turn && table[1][1] == turn && table[2][0] == turn) {
            return true;
        }
        return false;
    }

    private static void printResult() {
        if(checkDraw()) {
            System.out.println("DRAW!!!");
        }
        if(checkWin()) {
            System.out.println(turn + " Win!!!");
        }    
    }
    
}
